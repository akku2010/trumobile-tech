import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';

declare var $;

@IonicPage()
@Component({
  selector: 'page-booking-detail',
  templateUrl: './booking-detail.html',
})
export class BookingDetailPage {
  items: any;
  signature = '';
  isDrawing = false;
  @ViewChild('htmlData') htmlData: ElementRef;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 2,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };
  USERS = [
    {
      "id": 1,
      "name": "Leanne Graham",
      "email": "sincere@april.biz",
      "phone": "1-770-736-8031 x56442"
    },
    {
      "id": 2,
      "name": "Ervin Howell",
      "email": "shanna@melissa.tv",
      "phone": "010-692-6593 x09125"
    },
    {
      "id": 3,
      "name": "Clementine Bauch",
      "email": "nathan@yesenia.net",
      "phone": "1-463-123-4447",
    },
    {
      "id": 4,
      "name": "Patricia Lebsack",
      "email": "julianne@kory.org",
      "phone": "493-170-9623 x156"
    },
    {
      "id": 5,
      "name": "Chelsey Dietrich",
      "email": "lucio@annie.ca",
      "phone": "(254)954-1289"
    },
    {
      "id": 6,
      "name": "Mrs. Dennis",
      "email": "karley@jasper.info",
      "phone": "1-477-935-8478 x6430"
    },
    {
      "id": 7,
      "name": "Leanne Graham",
      "email": "sincere@april.biz",
      "phone": "1-770-736-8031 x56442"
    },
    {
      "id": 8,
      "name": "Ervin Howell",
      "email": "shanna@melissa.tv",
      "phone": "010-692-6593 x09125"
    },
    {
      "id": 9,
      "name": "Clementine Bauch",
      "email": "nathan@yesenia.net",
      "phone": "1-463-123-4447",
    },
    {
      "id": 10,
      "name": "Patricia Lebsack",
      "email": "julianne@kory.org",
      "phone": "493-170-9623 x156"
    },
    {
      "id": 11,
      "name": "Chelsey Dietrich",
      "email": "lucio@annie.ca",
      "phone": "(254)954-1289"
    },
    {
      "id": 12,
      "name": "Mrs. Dennis",
      "email": "karley@jasper.info",
      "phone": "1-477-935-8478 x6430"
    },
    {
      "id": 13,
      "name": "Leanne Graham",
      "email": "sincere@april.biz",
      "phone": "1-770-736-8031 x56442"
    },
    {
      "id": 14,
      "name": "Ervin Howell",
      "email": "shanna@melissa.tv",
      "phone": "010-692-6593 x09125"
    },
    {
      "id": 15,
      "name": "Clementine Bauch",
      "email": "nathan@yesenia.net",
      "phone": "1-463-123-4447",
    },
    {
      "id": 16,
      "name": "Patricia Lebsack",
      "email": "julianne@kory.org",
      "phone": "493-170-9623 x156"
    },
    {
      "id": 17,
      "name": "Chelsey Dietrich",
      "email": "lucio@annie.ca",
      "phone": "(254)954-1289"
    },
    {
      "id": 18,
      "name": "Mrs. Dennis",
      "email": "karley@jasper.info",
      "phone": "1-477-935-8478 x6430"
    },
    {
      "id": 19,
      "name": "Leanne Graham",
      "email": "sincere@april.biz",
      "phone": "1-770-736-8031 x56442"
    },
    {
      "id": 20,
      "name": "Ervin Howell",
      "email": "shanna@melissa.tv",
      "phone": "010-692-6593 x09125"
    },
    {
      "id": 21,
      "name": "Clementine Bauch",
      "email": "nathan@yesenia.net",
      "phone": "1-463-123-4447",
    },
    {
      "id": 22,
      "name": "Patricia Lebsack",
      "email": "julianne@kory.org",
      "phone": "493-170-9623 x156"
    },
    {
      "id": 23,
      "name": "Chelsey Dietrich",
      "email": "lucio@annie.ca",
      "phone": "(254)954-1289"
    },
    {
      "id": 24,
      "name": "Mrs. Dennis",
      "email": "karley@jasper.info",
      "phone": "1-477-935-8478 x6430"
    },
    {
      "id": 25,
      "name": "Leanne Graham",
      "email": "sincere@april.biz",
      "phone": "1-770-736-8031 x56442"
    },
    {
      "id": 26,
      "name": "Ervin Howell",
      "email": "shanna@melissa.tv",
      "phone": "010-692-6593 x09125"
    },
    {
      "id": 27,
      "name": "Clementine Bauch",
      "email": "nathan@yesenia.net",
      "phone": "1-463-123-4447",
    },
    {
      "id": 28,
      "name": "Patricia Lebsack",
      "email": "julianne@kory.org",
      "phone": "493-170-9623 x156"
    },
    {
      "id": 29,
      "name": "Chelsey Dietrich",
      "email": "lucio@annie.ca",
      "phone": "(254)954-1289"
    },
    {
      "id": 30,
      "name": "Mrs. Dennis",
      "email": "karley@jasper.info",
      "phone": "1-477-935-8478 x6430"
    }
  ];
  content_one: string;
  content_two: string;
  content_three: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    public file: File,
    public socialSharing: SocialSharing,
    public storage: Storage,
    private sanitizer: DomSanitizer,
    private toastCtrl: ToastController
  ) {
    this.items = navParams.get('params');
    console.log("navigated items: ", this.items);
  }
  tempOne: string; tempTwo: string; tempThree: string; tempFour: string; tempFive: string; tempSix: string;
  ionViewDidEnter() {

    console.log('ionViewDidLoad BookingDetailPage');
    // this.signaturePad.clear();
    this.storage.remove('savedSignature');
    // this.storage.get('savedSignature').then((data) => {
    //   this.signature = data;
    // });

    this.content_one = `<span style="font-weight: 600;">Rent</span>:It covers the period contracted. <span
    style="font-weight: 600;">Helmet</span>: The use of it is required. Passengers: On scooters or
  motorcycles in adition to the driver cannot travel as occupiers more people than allow his driver licence or
  circulation permit and always with protecion helmet .Otherwise it is illegal, dangerous, damaging the
  vehicle and cancels the insurance. If this happens the contract will be canceled and will lose the money
  given. <span style="font-weight: 600;">Theft</span> :In case of theft, will lose the amount paid without
  prejudice to other actions until the
  recovery of the total value of the vehicle.
  <span style="font-weight: 600;">Time and place of delivery</span> : Failure to return to the time and place
  will be penalized with an additional
  charge depending on current rates. <span style="font-weight: 600;">Sea area/Sport driving</span>: Driving in
  maritime areas or sport competitions,
  official or not it is illegal, damaging the vehicle, voids the contract and insurance and causes the loss of
  the amount paid without predujice to others actions until recovery total value of the vehicle.<br />
  <span style="font-weight: 600;">DO NOT LEAVE OBJECTS INSIDE AND CLOSE THE VEHICLE.IT IS OBLIGATORY TO CARRY
  OUT THIS CONTRACT IN THE
  VEHICLE.THE LESSEE DECLARES HAVING READ AND AGREED WITH ALL THE CONDITIONS PLASMED IN THIS AGREEMENT,
  INCLUDING THE REVERSE/S, THAT ALL THE INDICATED DATA ARE CERTAIN AND DECLARES TO BE IN</span><br/>
  `;

    this.tempOne = `<span style="font-weight: 600;">  POSSESSION OF THE
  NECESSARY LICENSE FOR THE DRIVING OF THIS VEHICLE. THE FAILURE TO BE CERTAIN THE DATA PROVIDED OR NOT IN
  POSSESSION OF THE LICENSE TO DRIVE THIS VEHICLE CARRIES THE LOSS OF THE CONTRIBUTION BONDED. THE CONTRACTOR
  ACCEPTS, WITH THE SIGNING OF THIS AGREEMENT, ITS LIABILITY IF THERE WERE LOSS, SUBSTITUTION / THEFT, DAMAGE
  OR
  DAMAGE TO THE VEHICLE AND COVER THE TOTAL VALUE OF THE VEHICLE IN THE EVENT THAT THE AMOUNT DELIVERED AS A
  DEPOSIT IS INSUFFICIENT. AUTHORIZING ALQUILOSCOOTER TO CHARGE ON YOUR CREDIT CARD ANY ADDITIONAL EXPENDITURE
  FOR THIS OR OTHER CONCEPTS THAT I HAVE.
  THE LOSS, BREAK OR DAMAGE OF THE HELMETS WILL BE PENALIZED WITH 50€ PER UNIT</span>`;

    this.content_two = `Similarly, in compliance with the provisions of LOPD 15/99 and RLOPD, we inform that the data Personal
    voluntarily provided by you will included in the files responsibility of <span style="font-weight: 600;">GRUPO
      SERVYTUR S.C</span>, registered at the
    General Registry Data Protection, whose purpose, is to manage the notifying the insurer of vehicle use leased
    for you, for purposes of administrative, accounting and management commercial of the company, as well as the
    compliance with any legal obligation and corporate purpose of the company .
    With the signing of this, you pay your express consent that their data can be transferred to third parties
    directly related when it necessary for any of the purposes set in the previous paragraph. You may request free
    the exercise of rights of access, rectification, opposition and/or cancellation writing certificate email to
    info@alquiloscooter.com attaching, in any case, proof of his identity.`;


    this.content_three = `The contracting company, hereinafter ALQUILOSCOOTER, rent the customer whose data and signature appear on this
contract, the vehicle designated in accordance with the
terms and conditions set out and the tenant accepts and agrees to comply. <br/>
<span style="font-weight: 600;">ARTICLE 1 . USE OF VEHICLE</span>: THE CLIENT agrees not to drive the vehicle
to let other people unless expressly
accepted by ALQUILOSCOOTER and, in any case,under the sole and personal
responsibility of the CUSTOMER. Likewise, the CLIENT agrees not to drive or allow the vehicle is driven: A)
For transport for valuable consideration ofpassengers or goods, whatever the
class chosen remuneration and form, written or verbal agreement. B) To push or tow any vehicle or trailer or
any object, rolling or not. C) To participate in sports competitions or No.D) For
anyone who is in poor condition to do so safely, andeither by alcohol ingestion, drugs, drugs, diseases,
fatigue either by any other similar or analogous circumstance which reduces their
drivability. E) purposes illegal or for transporting goods. F) Overloaded, ie, transporting passengers than
permitted or authorized number.G) For persons other than expressly mentioned
below and previously authorized by the customer, provided that such persons have attained 23 years they hold a
driving license inforce for a minimum period of three years and are in
possession of it. Such people are the
signatory of the contract and the person / s designated by him in the contract.`;

    this.tempTwo = ` <br/><br/>
 H)Negligently, ie, the CUSTOMER is obliged to maintain the closed
vehicle safely and to personally keep their keys and documents, not you should leave it. I) As itself, ie,
that may not assign, sell, mortgage or pledge this contract, the vehicle, its equipment
or tools, or treat any so that would be prejudicial to ALQUILOSCOOTER. Any violation of these conditions
authorize the CUSTOMER ALQUILOSCOOTER to demand the immediate return
ofvehicle, without justification or compensation.<br/>
<span style="font-weight: 600;">ARTICLE 2o STATE ANDIDENTIFICATION OF VEHICLE</span>: The CUSTOMER
acknowledges that it has received the vehicle in
perfect state is marching and cleaning, includes tyres, wich incase of
damage not due to normal wear and tear, THE CUSTOMER must immediaty replace, at it is expense by another/s with
identical characteristics and state or pay the amountand damage to the
vehicle. In any circumstances may THE CUSTOMER manipulate counters mileage or other, or their ducts and should
he do respond , to the damage caused to the vehicle. Also THE CUSTOMER
exonerates ALQUILOSCOOTER for any liability for loss or damage caused in the objetcs left or transported in
the vehicle by THE CUSTOMER or any other person or clothing of these either
during the contract period or after the return of the vehicle. THE CUSTOMER deffends and if necessary,
indemnify ALQUILOSCOOTER allclaims based on such assumptions .<span style="font-weight: 600;">THE VEHICLE
  OBJETC OF THIS AGREEMENT IS DELIVERED TO THE CUSTOMER WITH VINYL STICKERS LABELING WITH THE TRADEMARK
  ALQUILOSCOOTER .COM AND DIFFERENT VINYLS WITH RELATED
  TO NUMBER OF UNITY ON FLEET AND VYNILS WITH TEXT "RENT A BIKE". THE VEHICLE MUST BE RETURNED WITH ALL
  WEARING
  VYNILS STICKERS WHEN WAS DELIVERED TO THE
  CUSTOMER. THE LACK OF ANY VYNIL STICKER OR INCLUSION THE SOMEONE ELSE WILLMEAN THE LOSS OF DEPOSIT.</span><br/>
<span style="font-weight: 600;">ARTICLE 3o. PRICE RENT, DEPOSIT AND EXTENSION</span> : The rental price and
the reservation is determined by the
current tariffs . In any case the reserve may serve to prolong thelease . If the CUSTOMER wants to keep the
vehicle longer than originally agreed, you must obtain written ALQUILOSCOOTER, whom he must pay the
corresponding amount as it may otherwise be claimed by misappropriation or similar figure authorization . The
CUSTOMER is obliged to return the vehicle to ALQUILOSCOOTER on the date specified in the lease. Only return
the vehicle to ALQUILOSCOOTER at the place and date agreed in the rental terminating the contract .
<span style="font-weight: 600;">DELAY IN RETURNING THE VEHICLE OF MORE THAN 30M AUTHORIZE ALQUILOSCOOTER EXTRA
  CHARGE TO COLLECT A DAY OF
  NORMAL RATE OF THE VEHICLE . IF EXCEED THE DAY 24 H
  STATED FOR THE RETURN OF THE VEHICLE WILLMEAN THE LOSS OF DEPOSIT AND ALQUILOSCOOTER CAN REMOVE THE SAME IF
  FOUND . THEY CAN ALSO START ADEQUATE LEGAL AND
  POLICE ACTIONS TO RECOVERY VEHICLE . EVERYDAY PASSING VEHICLE RECOVERY TO BE CHARGED TO THE CUSTOMER FOR
  NORMAL DAILY RATE . THE RETURN BY CUSTOMER VEHICLE
  BEFORE THE END OF PERIOD INITIALLY CONTRACTED TOTAL WILL MEAN THE LOSS OF THE DEPOSIT, AND THE RATE
  CONTRACTED
  AND WILL APPLY THE NORMAL RATE DAILY .
  A BREACH BY CUSTOMER FOR THE PERIOD HIRED WILL MEAN THE IMMEDIATE LOSS OF DEPOSIT, CONTRACTED RATE WILL VOID
  AND WILL APPLY THE NORMAL RATE DAILY.</span><br/>
<span style="font-weight: 600;">ARTICLE 4o . PAYMENTS</span> . The CLIENT agrees to pay ALQUILOSCOOTER : A )
The corresponding amounts as rate,
duration of rental . B ) An amount by the number of kilometers covered
duringthe rental period, calculated at the rate in force, if it provides as payment an amount per kilometer.
The number of kilometers traveled during the rental period will be indicated
bythe meter installed in the vehicle by the manufacturer. If the counter has not worked, whatever the cause,
the number of kilometers is calculated on the basis of 300 km / day rental.C)
An additional amount for service between different cities if such is the case or if the vehicle is left in a different place than expected, without written consent of ALQUILOSCOOTER,
corresponding to a mileage allowance or lump sum specified in the applicable rate based on distance separating
the city where the vehicle has been returned to the city which hasmade
the rent.
`;

    this.tempThree = `<br/>
D) Any taxes, fees and contributions, direct or indirect, imposed on the amounts mentioned in
paragraphs A), B), C). E) Any fines, expenses, disbursements and taxesfor any
breaches of legislation on the movement, driving, parking and other corresponding to the vehicle, the CUSTOMER
or ALQUILOSCOOTER during the term of this contract,except
infringements attributable to ALQUILOSCOOTERE) Any fines, expenses, disbursements and taxes for any breaches
of legislation on the movement, driving, parking and othercorresponding
to the vehicle, the CUSTOMER or ALQUILOSCOOTER during the term of this contract, except infringements
attributable to ALQUILOSCOOTER . F) Disbursements to bemade for the CLIENT
ALQUILOSCOOTER payments due under this contract, judicial and extrajudicial. Payment must be made at the end
of this contract or within 24 hours, otherwise the amount due will
increase by 15% as a penalty clause..G) Expenses ALQUILOSCOOTER be performed to repair damage caused by
collision or otherwise, unless the customer has acted with due diligence
and responsible for the collision is a third party. The expenses must perform ALQUILOSCOOTER to repair the
damage to the vehicle for any other reason. H)The customer can not claim
total or partial exemption from responsibility with the intention to delay or refuse payment amounts owed to
ALQUILOSCOOTER. I) The CLIENT signing the contract may be obliged to
deliver 250€ as a deposit to ensure the return of the vehicle at the end of the lease in perfect condition,
without deterioration of some kind, and respond to loss or theft. This deposit also liable for the value of
the vehicle in case of accident.Loss of deposit for damage, loss, theft and/or loss occurred and is without
prejudice to any subsequent expense of responsibilities of the end customer, in the event that these exceed
250 €. For this purpose exclusively, the vehicles are valued at € 2,500. Said quantity of 250 € will be
liquidated at the end of the contract,
resulting in their return or not as the case.<br/>
<span style="font-weight: 600;">ARTICLE 5. INSURANCE</span>. GUARANTEES AND COVERAGE: Only the driver or
drivers specifically accepted by
ALQUILOSCOOTER have the quality of insured. CLIENT driver and all authorized
vehicle in accordance with Article 1 participate as insured in an insurance policy vehicle whose copy is
available to the CUSTOMER at the headquarters of ALQUILOSCOOTER . It said policy
covers civil liability and damages to third parties in accordance with the legislation in force in the country
of registration of the vehicle. The CLIENT agrees with said policy and undertakes
to respect the conditions and clauses. Furthermore, it is necessary to adopt all necessary measures to protect
the interests of ALQUILOSCOOTER and the insurance company in case of
accident and in particular, it undertakes measures to: A) Inform ALQUILOSCOOTER within 24 hours of any
accident, theft , fire damage and the police of any damage or theft produced.B)
To include in its declaration the circumstances, place, date, time of accident, name and address of witnesses,
name and address of the owner of the other vehicle, registration of this, and
insurance company and policy number. C) Attach to this statement any police report, Civil Guard or Court if
the case so requires. D) Do not argue in any case the responsibility or
compromise with third parties regarding the accident. E) Do not leave the accident vehicle without taking
appropriate measures to protect and safeguard measures. The contract has the
following insurance coverage and guarantees,in accordance with the provisions in the clauses of the contracted
policy with AXA INSURANCE: A) Compulsory liability insurance covers the
provisions of law regarding injuries and damage caused to third parties by the insured moped. B) Civil
liability of voluntary subscription up to 50 M deEuros.C) Legal defense and damage
claim covers, with the limitations established in the general Conditioning, legal, judicial and extrajudicial
assistance and payment of costs incurred for the arbitration legal defense of the
insured in administrative, judicial and derivatives accident circulation with the insured moped. D) Insurance
driver, severance pay death and health care authorized driver and legally
entitled, as a result of a traffic accident the insured scooter. Sums insured health yasistencia death in
June. 18000€ and 18000€ respectively. <span style="font-weight: 600;">E) Travel assistance with a
maximum�radius of 100km from the base of
ALQUILOSCOOTER. Any assistance that exceeds that from the base of
ALQUILOSCOOTER.</span>`;

    this.tempFour = ` <br/> <span style="font-weight: 600;"> Any assistance that exceeds that radius will be paid by the customer.</span>The warranty
    does not
    cover clothing or objects carried. Insurance is meant to refer only to the length of rental according to this
    contract. After this term, unless expressly accepted extension, ALQUILOSCOOTER disclaims any responsibility
    for accidents or damage caused by the customer, of which this is solely responsible. The insurance does not
    cover the driver is not in possession of valid license and driving force or not in good condition to drive
safely. When the driver is not in optimal driving conditions by ingestion of alcohol, drugs, medication or
illness or other similar cause, the client and the driver shall be jointly liable for any damages that may
experience ALQUILOSCOOTER. Damage caused to the vehicle as a result of uneven or poor condition of the road
will be attributable to the CUSTOMER. ALQUILOSCOOTER disclaims any responsibility for the accident on third
parties or damage to the vehicle that the CUSTOMER caused during the period if deliberately�supplied to
ALQUILOSCOOTER inaccurate information concerning your organization, address or validity of his driving license
and, if so, insurance will not be valid.<br/>
<span style="font-weight: 600;">ARTICLE 6th. THEFT . THE CUSTOMER BE LIABLE FOR VEHICLE THEFT OR
ABDUCTION AND DAMAGES TO THE SAME AS A RESULT
OF SUCH T THEFT OR MISAPPOPIATION.
CUSTOMER AUTHORIZES ALQUILOSCOOTER A CHARGING IN YOUR CREDIT CARD REQUIRED AMOUNT TO COVER THE VALUE OF
VEHICLE DISCOUNTING THE AMOUNT PAID AS
DEPOSIT.</span><br/>
<span style="font-weight: 600;">ARTICLE 7 . MAINTENANCE AND REPAIRS</span> : The normal mechanical wear of the
vehicle is taken ALQUILOSCOOTER . In
the case where the vehicle is immobilized, repairs may be made
only by written instructions and according ALQUILOSCOOTER, which must include a detailed invoice agreement .
Defective parts replaced must be submitted togetherwith the invoice
. In any case, the CUSTOMER may claim damages for delay in delivery of the vehicle, cancellation of rental or
immobilisation due to repairs during the rental .The CLIENT will not be
liable for personal injury or property damage caused by default construction or previous repairs .
<span style="font-weight: 600;">FOR LONG-TERM RENTS SUCH REVISIONS MUST BE MANDATORY EVERY 3,000 KM. IF ANY OF
THESE REVISIONS ARE NOT
FULFILLED, THEY WILL SUPPORT THE IMMEDIATE
LOSS OF THE SECURITY AND THAT THE COSTS OF ANY REPAIRS OF ANY KIND WILL BE ASSUMED BY THE CLIENT AND NOT
ALQUILOSCOOTER.</span><br/>
<span style="font-weight: 600;">ARTICLE 8. OIL AND GASOLINE</span>: Gasoline will be charged to the customer.
The CUSTOMER must constantly check the
water levels and oil level and tire pressure. If the customer has
to put oil (always ALQUILOSCOOTER prior authorization) shall submit an invoice for reimbursement for
replacement of oil.<br/>
<span style="font-weight: 600;">ARTICLE 9. LIABILITY</span>: The customer or expressly admitted drivers
respond criminally for offenses committed by
them during driving and parking it under the Criminal Code,
Highway Code and other applicable legal provisions.
<span style="font-weight: 600;">IN THE CASE OF ANY CRIME OR PENALTY ADMINISTRATIVE OR TRAFFIC THAT WERE
NOTIFIED TO ALQUILOSCOOTER AFTER
COMPLETION OF CONTRACT CUSTOMER
AUTHORIZE ALQUILOSCOOTER TO MAKE A CHARGE ON YOUR CREDIT CARD TO COLLECT THE AMOUNT OF SUCH VIOLATION OR
PUNISHMENT.</span><br/>
<span style="font-weight: 600;">ARTICLE 10o . Validity of the contract</span> . Any modification of the terms
and conditions of this contract shall
be expressed in writing, without which it will be null andwithout effect.<br/>
<span style="font-weight: 600;">ARTICLE 11o. APPLICABLE LEGISLATION </span>.This contract shall be governed by and construed in accordance with the
laws of the country has been signed , whose courts shall have
jurisdiction to adjudicate disputes arising from it, giving the parties to the jurisdiction that may
correspond . The issues arising in connection with this contract between the customer
and competition ALQUILOSCOOTER are the Courts of Malaga . The interpretation of this contract is subjetc to
the interpretation of text in Spanish .`;

    this.tempFive = `<br/><br/><br/>`;
  }

  public openPDF(): void {
    let DATA = this.htmlData.nativeElement;
    let doc = new jsPDF('l', 'pt', 'a4');

    // doc.fromHTML(DATA.innerHTML,15,15);
    doc.fromHTML(DATA.innerHTML, 15, 15, {
      'width': 595
    },
      function () {
        doc.save('thisMotion.pdf');
      });
  }


  public downloadPDF(): void {
    let DATA = this.htmlData.nativeElement;
    let doc = new jsPDF('p', 'pt', 'a4');

    let handleElement = {
      '#editor': function (element, renderer) {
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML, 15, 15, {
      'width': 200,
      'elementHandlers': handleElement
    });

    doc.save('angular-demo.pdf');
  }

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }

  savePad() {
    this.signature = this.signaturePad.toDataURL();
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    // let toast = this.toastCtrl.create({
    //   message: 'New Signature saved.',
    //   duration: 3000
    // });
    // toast.present();
  }

  clearPad() {
    debugger
    this.signaturePad.clear();
  }

  OnExport() {
    // this.OnExportPDF();
    this.getPDF();
  }
  loading: any;
  presentLoading(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    return this.loading.present();
  }

  getPDF() {
    debugger
    if (this.signature === '') {
      this.toastCtrl.create({
        message: "Please provide customer's signature.. and try export again.",
        position: 'middle',
        duration: 2500
      }).present();
      return;
    }
    let self = this;
    this.presentLoading('Creating PDF file...');
    // var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var HTML_Width = 595;
    var top_left_margin = 5;
    var PDF_Width = HTML_Width + (top_left_margin * 2);
    var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;


    html2canvas($(".canvas_div_pdf")[0], { allowTaint: true, useCORS: true }).then(function (canvas) {
      canvas.getContext('2d');

      console.log(canvas.height + "  " + canvas.width);

      /////////////////////////////////////////////////////////
      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
      // var pdf = new jsPDF('p', 'mm', 'a4');
      pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
      }

      pdf.save("HTML-Document.pdf");
      self.loading.dismiss();
      let pdfOutput = pdf.output();
      // ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }
      //PDF file will stored , you can change this line as you like
      const directory = self.file.dataDirectory;
      const fileName = "booking_confirmed_download.pdf";

      self.file.checkFile(directory, fileName).then((success) => {
        //Writing File to Device
        self.file.writeFile(directory, fileName, buffer, { replace: true })
          .then((success) => {
            self.loading.dismiss();
            console.log("File created Succesfully" + JSON.stringify(success));
            self.socialSharing.share('PDF file export', 'PDF export', success['nativeURL'], '')

          })
          .catch((error) => {
            self.loading.dismiss();
            console.log("Cannot Create File " + JSON.stringify(error));
          });
      })
        .catch((error) => {

          //Writing File to Device
          self.file.writeFile(self.file.dataDirectory, "booking_confirmed_download.pdf", buffer, { replace: true })
            // self.file.writeFile(directory, fileName, buffer, { replace: true })
            .then((success) => {
              self.loading.dismiss();
              console.log("File created Succesfully" + JSON.stringify(success));
              // alert("File created Succesfully" + JSON.stringify(success));
              ///////////
              self.socialSharing.share('PDF file export', 'PDF export', success['nativeURL'], '')
              ///////////
            })
            .catch((error) => {
              self.loading.dismiss();
              console.log("Cannot Create File " + JSON.stringify(error));
            });
        });
    });
  };


}
